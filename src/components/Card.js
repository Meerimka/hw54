import React from 'react';
import './Card.css';

const Card = props=>{
    return(
        <div className={"Card Card-rank-" +  props.rank + " Card-" +props.suit}>
            <span className={"Card-rank-" + props.rank}>{props.rank}</span>
            <span className={"Card-" + props.suit}>{props.suit}</span>
        </div>
        )

};




export default Card;