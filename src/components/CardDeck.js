class CardDeck {
    constructor(){
        this.suits = ["d", "c", "h", "s"];
        this.ranks = [
              "a",
              "2",
              "3",
              "4",
              "5",
              "6",
              "7",
              "8",
              "9",
              "10",
              "j",
              "q",
              "k"];

        this.allCards = [];
        for(let i = 0; i < this.ranks.length; i++) {
            for(let j = 0; j < this.suits.length; j ++) {
                const card = {rank: this.ranks[i], suit: this.suits[j]};
                this.allCards.push(card);
            }

        }
        console.log(this.allCards);

    }
    getCard = ()=>{
        const index = Math.floor(Math.random() * this.allCards.length);
        const randomCard = this.allCards[index];
        this.allCards.splice(index, 1);
        return randomCard
    };
    getCards = (howMany)=>{
        const newCards = [];
        for(let i=0; i< howMany; i++){
            newCards.push(this.getCard());
        }
        return newCards;

    }}


export default CardDeck;