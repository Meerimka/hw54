import React, { Component } from 'react';

import './App.css';
import Card from "./components/Card";
import CardDeck from './components/CardDeck';
import PockerHand from './components/PockerHand';


class App extends Component {

constructor () {
    super();
    this.state = {
        cards: []
    };
}
    clickHandler = () => {
        const cards = new CardDeck();
        const fiveCards = cards.getCards(5);
        this.setState({cards:fiveCards})
        const poker = new PockerHand();
        const combination = poker.getOutCome(fiveCards);
        console.log(combination)
    };

  render() {
    return (
      <div className="App">
          <button onClick={this.clickHandler}>click</button>
          {this.state.cards.map((card, index)=>{
              return (
                  <Card rank={card.rank} suit={card.suit}
                  />
              )
          })}
      </div>
    );
  }
}

export default App;
